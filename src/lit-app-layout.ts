import { LitElement, html, customElement, property } from '@polymer/lit-element'
import '@polymer/app-layout/app-layout'

export enum AppLayoutMode {
	/**
	 * Simple app layout with a header, toolbar and the content.
	 */
	BaseActivity = "base"
}

/**
 * 
 * Slots: (Some activities may have more ore less slots)
 * - toolbar-title
 * - toolbar-content
 * - drawer-content
 * - header-content
 * - view-content
 */
@customElement("lit-app-layout" as any)
export class LitAppLayoutElement extends LitElement {

	/**
	 * The activity mode to render.
	 */
	@property({ type: String })
	public layout: AppLayoutMode = AppLayoutMode.BaseActivity;


	protected renderBaseActivity() {
		return html`
			<style>

			</style>
			<app-header-layout fullbleed>
				<app-header reveals>
					<app-toolbar>
						<slot name="toolbar-content-pre"></slot>
						<div main-title>
							<slot name="toolbar-title"></slot>
						</div>
						<slot name="toolbar-content"></slot>
					</app-toolbar>
					<slot name="header-content"></slot>
				</app-header>
				<slot name="view-content"></slot>
			</app-header-layout>
		`;
	}

	// ... more coming soon. Or later

	protected render() {
		switch (this.layout) {
			// Fallback to base activity
			default:
				return this.renderBaseActivity();
		}
	}

}

declare global {
	interface HTMLElementTagNameMap {
		"lit-app-layout": LitAppLayoutElement
	}
}